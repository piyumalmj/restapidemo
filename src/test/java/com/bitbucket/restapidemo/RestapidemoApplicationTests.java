package com.bitbucket.restapidemo;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class RestapidemoApplicationTests {

    WebDriver driver;
    @FindBy(xpath = "/html/body/h1")
    WebElement hello_world;

    @BeforeAll
    public static void browser_open() {

        WebDriverManager.chromedriver().setup();

        //Create Chrome Options
        ChromeOptions options = new ChromeOptions();
        DesiredCapabilities capabilities = new DesiredCapabilities();
        options.addArguments("--headless");
        options.addArguments("--test-type");
        options.addArguments("--disable-popup-bloacking");
        options.addArguments("--disable-infobars");
        options.addArguments("--headless");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--ignore-certificate-errors");
        options.addArguments("--blink-settings=imagesEnabled=false");
        options.addArguments("--disable-gpu");
        options.addArguments("--add-host host.docker.internal:host-gateway");
        options.addArguments("--start-maximized");
        options.addArguments("--disable-web-security");
        options.addArguments("--disable-browser-side-navigation");
        options.addArguments("--disable-notifications");
        options.addArguments("--remote-debugging-port=9222");
        options.addArguments("--disable-extensions");
        options.addArguments("--allow-running-insecure-content");
        options.addArguments("--allow-insecure-localhost");
        options.addArguments("--window-size=1024,768");
        System.setProperty("webdriver.chrome.args", "--disable-logging");
        System.setProperty("webdriver.chrome.silentOutput", "true");
        options.setPageLoadStrategy(PageLoadStrategy.NONE);
        options.setExperimentalOption("useAutomationExtension", false);

//        DesiredCapabilities chrome = DesiredCapabilities.chrome();
//        chrome.setJavascriptEnabled(true);
        capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        options.setCapability(ChromeOptions.CAPABILITY, options);
    }

    @BeforeEach
    void setupTest() {
        driver = new ChromeDriver();
    }

    @AfterEach
    void teardown() {
        driver.quit();
    }

    @Test
    void contextLoads() {
        driver.navigate().to("http://localhost:9090/api/hello");
        hello_world.isDisplayed();
    }

}
