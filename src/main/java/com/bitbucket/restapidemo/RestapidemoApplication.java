package com.bitbucket.restapidemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.net.InetAddress;
import java.net.UnknownHostException;

@SpringBootApplication
public class RestapidemoApplication {

    public static void main(String[] args) throws UnknownHostException {
        System.out.println(InetAddress.getLocalHost());
        SpringApplication.run(RestapidemoApplication.class, args);
        System.out.println(InetAddress.getLocalHost());
    }



}
